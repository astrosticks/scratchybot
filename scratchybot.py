import argparse
import html
import json
import os
import random
import re
import urllib.request

from twitter import Twitter, OAuth

import ScratchNativeRender

MENTION_RE = '(?<=\\W)@\\w+'  # matches cases like `@username`
LINK_RE = '((\\w+:\\/\\/\\S+)|(\\w+[\\.:]\\w+\\S+))[^\\s,\\.]'  # matches cases like www.google.com

POST_TYPES = [
    'project_comment',
    'project_thumbnail',
]
DEFAULT_POST_TYPE = 'project_comment'

FRONT_PAGE_CATEGORIES = {
    'newest': 'community_newest_projects',
    'top_remixed': 'community_most_remixed_projects',
    'design_studio': 'scratch_design_studio',
    'curated': 'curator_top_projects',
    # 'featured_studios': 'community_featured_studios',
    'top_loved': 'community_most_loved_projects',
    'featured': 'community_featured_projects',
}
DEFAULT_FRONT_PAGE_COMMENTS_SOURCE = 'top_loved'
DEFAULT_FRONT_PAGE_THUMBNAIL_SOURCE = 'newest'

EXAMPLE_SOURCE = []

config = json.loads(open('./config.json','r').read())

def getJsonFromUrl(url:str):
    with urllib.request.urlopen(url) as response:
        response_body = response.read()
    return json.loads(response_body)

def getImageDataFromUrl(url:str):
    return urllib.request.urlopen(url).read()

# Load data from the front page
front_page_data = getJsonFromUrl('https://api.scratch.mit.edu/proxy/featured')

def sanitize_scratch_text(text:str):

    # remove whitespace on ends
    text = text.strip()

    # Escape HTML character codes
    text = html.unescape(text)

    if config['hideMentions']:
        text = re.sub(MENTION_RE, "[user]", text)
    if config['hideLinks']:
        text = re.sub(LINK_RE, "[link]", text)

    return text

class ScratchyBotPost:

    def __init__(
        self, 
        text_body:str, 
        validation_string:str, validation_args:dict={},
        media_url:str=None, media_alt_text:str='', 
        render_template:str=None, render_alt_text:str='', render_kwargs:dict=None,
    ):
        
        self._text_body = text_body
        
        self._media_url = media_url
        self._media_alt_text = media_alt_text

        if media_url is not None and len(media_alt_text) == 0:
            raise Exception('media_alt_text must be provided with media_url')

        self._render_template = render_template
        self._render_alt_text = render_alt_text
        self._render_kwargs = render_kwargs

        if render_template is not None and len(render_alt_text) == 0:
            raise Exception('render_alt_text must be provided with render_template')

        self._validation_string = validation_string
        self._validation_args = validation_args

        self.text = None
        self.media_data = None
        self.alt_text = None

    def validate(self):
        """Test self against attributes set in self._validation_args"""

        val_str = self._validation_string
        val_args = self._validation_args

        if 'max_characters' in val_args and len(val_str) > val_args['max_characters']:
            return False
        
        if 'min_words' in val_args and len(val_str.split()) < val_args['min_words']:
            return False
        
        if 'allow_remixes' in val_args and not val_args['allow_remixes'] and val_str.lower().endswith('remix'):
            return False

        return True
    
    def loadMedia(self, render:bool=False):

        if render:
            self.text = ''
            self.media_data = ScratchNativeRender.renderPost(self)
            self.alt_text = self._render_alt_text

        else:
            self.text = self._text_body
            if self._media_url is not None:
                self.media_data = getImageDataFromUrl(self._media_url)
                self.alt_text = self._media_alt_text
        

def get_front_page_projects(n:int=20,source=DEFAULT_FRONT_PAGE_COMMENTS_SOURCE):
    # Of the projects on the front page, pick n at random, weighted by the number of loves it has
    # Even if all of the projects have 0 loves, random.choices will still choose a project
    projects = front_page_data[FRONT_PAGE_CATEGORIES[source]]
    k = min(n, len(projects))
    weights = map(lambda p: max(p['love_count'],1), projects)
    weighted_pick = random.choices(projects,weights=weights,k=k)
    return weighted_pick

def random_project_id(source=DEFAULT_FRONT_PAGE_COMMENTS_SOURCE):
    """Picks a project id from the front page"""
    return get_front_page_projects(1,source)[0]['id']

def get_project_author(project_id):
    """Requests the username of the author of a Scratch project"""

    project_info = getJsonFromUrl('https://api.scratch.mit.edu/projects/{}'.format(project_id))
    return project_info['author']['username']

def fetch_comments_as_posts():
    """Return a list of all comments under a project that satisfy the rules enabled in config.json"""

    if len(EXAMPLE_SOURCE) > 0:
        comments = EXAMPLE_SOURCE
    else:
        project_id = random_project_id()
        author = get_project_author(project_id)
        comments = getJsonFromUrl('https://api.scratch.mit.edu/users/{}/projects/{}/comments?offset=0&limit=40'.format(author, project_id))

    comment_valid_args = {
        'min_words': config['minWords'],
        'max_char': config['maxChar'],
    }

    posts = []
    for comment in comments:
        sanitized_comment = sanitize_scratch_text(comment['content'])
        post = ScratchyBotPost(
            text_body=sanitized_comment,
            validation_string=sanitized_comment,
            validation_args=comment_valid_args,
            render_template='project_comment',
            render_alt_text='Screenshot of Scratch comment that reads "{}"'.format(sanitized_comment),
            render_kwargs={
                'avatar__url': comment['author']['image'],
                'comment_body': sanitized_comment,
            },
        )
        posts.append(post)

    return posts

def fetch_projects_as_posts(source=DEFAULT_FRONT_PAGE_THUMBNAIL_SOURCE):

    projects = get_front_page_projects(source=source)

    project_valid_args = {
        'allow_remixes': False,
    }

    posts = []
    for project in projects:
        sanitized_title = sanitize_scratch_text(project['title'])
        thumbnail_url = 'https:'+project['thumbnail_url']
        post = ScratchyBotPost(
            text_body=sanitized_title,
            validation_string=sanitized_title,
            validation_args=project_valid_args,
            media_url=thumbnail_url,
            media_alt_text='Scratch project thumbnail',
            render_template='project_thumbnail',
            render_alt_text='Thumbnail of Scratch project, titled "{}"'.format(sanitized_title),
            render_kwargs={
                'thumbnail__url': thumbnail_url,
                'project_title': sanitized_title
            }
        )
        posts.append(post)

    return posts

def fetch_posts(post_type=DEFAULT_POST_TYPE):

    posts = fetch_comments_as_posts() if post_type == 'project_comment' else fetch_projects_as_posts()

    valid_posts = filter(lambda post: post.validate(), posts)

    return list(valid_posts)

def handleArgs():
    """Handles and asserts over command line arguments. Returns an argparse.args object."""
    parser = argparse.ArgumentParser(
        description='Finds random comments from scratch.mit.edu and posts them to Twitter.',
    )

    # Setup arguments
    parser.add_argument(
        '-t', '--type', 
        help='Which kind of content to find',
        choices=POST_TYPES,
        default='comment',
    )
    number_posts = parser.add_argument(
        '-n', '--number', 
        help='Number of posts to create',
        type=int, 
        default=1,
    )
    parser.add_argument(
        '-r', '--render', 
        help='Visualize response as it would appear on the Scratch website',
        action='store_const', 
        const=True, 
        default=False,
    )
    parser.add_argument(
        '--no-auth', 
        help='Disable Twitter authentication and status updates', 
        action='store_const', 
        const=True, 
        default=False,
    )
    parser.add_argument(
        '-s', '--source', 
        help='Which projects to pick from', 
        choices=FRONT_PAGE_CATEGORIES.keys(), 
        default="top_loved",
    )
    parser.add_argument(
        '-e', '--example', 
        help='Use preloaded data, for testing',
        action='store_const',
        const=True, 
        default=False,
    )

    args = parser.parse_args()

    # Temporary way to expand scope of args, will move argument handling to higher level at some point
    if args.example:
        with open('example_sources.json') as ef:
            EXAMPLE_SOURCE.append(json.load(ef)[args.type])

    # Assert over arguments
    try:
        assert args.number > 0
    except AssertionError:
        raise argparse.ArgumentError(number_posts, 'must be an integer greater than zero (got {})'.format(args.number))
    
    return args

def postToTwitter(post:ScratchyBotPost):

    # Load twitter credentials from environment variables
    ENV_API_KEYS = [
        'SCRATCHYBOT_TWITTER_ACCESS_TOKEN',
        'SCRATCHYBOT_TWITTER_ACCESS_TOKEN_SECRET',
        'SCRATCHYBOT_TWITTER_CONSUMER_KEY',
        'SCRATCHYBOT_TWITTER_CONSUMER_SECRET',
    ]
    env_api_values = list(map(lambda k: os.environ[k], ENV_API_KEYS))

    # Authentication
    auth = OAuth(*env_api_values)
    tw = Twitter(auth=auth)

    if post.media_data is None:
        # Post new status
        tw.statuses.update(status=post.text)

    else:
        # Upload image and post status
        tw_upload = Twitter(domain='upload.twitter.com', auth=auth) # authenticate with media server
        id_img = tw_upload.media.upload(media=post.media_data)['media_id_string'] # upload image data
        tw_upload.media.metadata.create(media_id=id_img, alt_text=post.alt_text) # attach alt text

        tw.statuses.update(status=post.text, media_ids=id_img)

def _main():
    """Invoked when executed via the command line"""

    args = handleArgs()

    valid_posts_counter = 0
    while valid_posts_counter < args.number:

        # TODO merge this logic with (and rename) fetch_posts
        found_posts = list()
        while len(found_posts) == 0:
            found_posts = fetch_posts(args.type)
        post = random.choice(found_posts)
        post.loadMedia(render=args.render)

        if args.no_auth:
            print('text:', post.text)
            if post.media_data is not None:
                print('alt_text:', post.alt_text)
                with open('render.png','w+b') as f:
                    f.truncate()
                    f.write(post.media_data)
                print('image written to render.png')
        else:
            postToTwitter(post)

        valid_posts_counter += 1

if __name__ == '__main__':
    _main()
