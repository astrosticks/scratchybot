import os
import html
import base64
import json

from selenium import webdriver

from jinja2 import Environment, FileSystemLoader, select_autoescape, environmentfilter

from scratchybot import ScratchyBotPost, getImageDataFromUrl

DEFAULT_CONFIG_PATH = 'render/config.json'
TEMPLATES_PATH = 'render/templates/'

render_config = json.load(open(DEFAULT_CONFIG_PATH))

_render_params = {
  'background_color': 'white',
  'container': {
    'max_aspect_ratio': 2.0,
    'width': 350,
  },
  'render': {
    'width': 600,
    'padding': {
      'top': 30,
      'bottom': 30,
      'left': 43,
      'right': 73,
    },
  },
}

_jinja_env = Environment(
  loader=FileSystemLoader(TEMPLATES_PATH)
)
# _jinja_env.filters['px'] = lambda x: '{}px'.format(x)
_jinja_env.filters['pxRender'] = lambda x: '{}px'.format(int(x)*(_render_params['container']['width']/_render_params['render']['width']))

_INSPECT_MODE = False

def renderPost(post:ScratchyBotPost):
  """Renders a post as it would appear on the Scratch website, returns a bytes object of PNG data"""

  # keep track of values to pass to jinja templates
  render_params = dict()

  # Merge template config with default
  render_params['config'] = render_config['default']
  template_config = render_config[post._render_template] if post._render_template in render_config else {}
  for key in template_config:
    render_params['config'][key] = template_config[key]

  # Process data from ScratchyBotPost object to include in render_params
  kwargs = post._render_kwargs
  content_data = {}
  for kwarg in post._render_kwargs:
    # if a URL is provided, download it and encode it into b64
    if kwarg.endswith('__url'):
      url_data = getImageDataFromUrl(kwargs[kwarg])
      kwarg_tag = kwarg[:-len('__url')] + '__b64'
      content_data[kwarg_tag] = base64.b64encode(url_data).decode()
    else:
      content_data[kwarg] = html.escape(kwargs[kwarg])
  render_params['content'] = content_data

  # Create HTML from template
  template = _jinja_env.get_template(post._render_template+'/template.html.jinja')
  rendered_html = template.render(**render_params)
  encoded_html = base64.b64encode(rendered_html.encode('utf-8')).decode()

  # Open Firefox at data URL to HTML
  ff_opt = webdriver.FirefoxOptions()
  if not _INSPECT_MODE:
    ff_opt.set_headless()
  browser = webdriver.Firefox(firefox_options=ff_opt)
  browser.get('data:text/html;base64,{}'.format(encoded_html))

  # Capture screenshot of target element
  screenshot_element = browser.find_element_by_id('scratchy-screenshot-target')
  screenshot_data = screenshot_element.screenshot_as_png

  # Print info about screenshot
  rect = screenshot_element.rect
  print('rendered as W{}*H{} image'.format(int(rect['width']),int(rect['height'])))

  if not _INSPECT_MODE:
    browser.close()

  return screenshot_data