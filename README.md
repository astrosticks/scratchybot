# @scratchybot v1.2

A python script that posts random comments from https://scratch.mit.edu to a Twitter account.

Scratch is a website that introduces young learners to the basics of programming. Users can post comments underneath their friends' projects with suggestions, friendly conversation, or complete nonsense. This Twitter bot serves to immortalize the wise words of these users.

## Dependencies

Dependencies required to work are listed in `requirements.txt`. See https://pip.pypa.io/en/latest/user_guide/#requirements-files for instructions on how to install.

## Usage

First, create a Twitter application at https://developer.twitter.com/en/apply-for-access. Then, set the environment variables:
- `SCRATCHYBOT_CONSUMER_KEY` (API key)
- `SCRATCHYBOT_CONSUMER_SECRET` (API secret key)
- `SCRATCHYBOT_ACCESS_TOKEN`
- `SCRATCHYBOT_ACCESS_TOKEN_SECRET`

Once your keys are set, run `./scratchybot.py` to tweet a random Scratch comment. See `./scratchybot.py --help` for more options.

### Credits

* Created by **David Gene** - https://astrosticks.com
